using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTestComponent : MonoBehaviour
{
    public AudioSource source;
    public AudioClip clip;
    public AudioClip clip1;
    public AudioClip clip2;
    public AudioClip clip3;
    public AudioClip clip4;
    public AudioClip clip5;
    public AudioClip clip6;
    public AudioClip clip7;
    public AudioClip clip8;
    public AudioClip clip9;
    public AudioClip clip10;
    public AudioClip clip11;
    private Vector3 yeet = new Vector3(-100f, -100f, -100f);


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 0)
        {
            source.PlayOneShot(clip);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 1)
        {
            source.PlayOneShot(clip1);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 2)
        {
            source.PlayOneShot(clip2);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 3)
        {
            source.PlayOneShot(clip3);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 4)
        {
            source.PlayOneShot(clip4);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 5)
        {
            source.PlayOneShot(clip5);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 6)
        {
            source.PlayOneShot(clip6);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 7)
        {
            source.PlayOneShot(clip7);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
        }


        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 8)
        {
            source.PlayOneShot(clip8);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 9)
        {
            source.PlayOneShot(clip9);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 10)
        {
            source.PlayOneShot(clip10);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 11)
        {
            source.PlayOneShot(clip11);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;

        }


    }
}
