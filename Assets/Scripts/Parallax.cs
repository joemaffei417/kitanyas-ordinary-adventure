using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public Camera cameraMain;
    public Transform player;

    Vector2 startPos;

    float startZ;

    Vector2 travel => (Vector2)cameraMain.transform.position - startPos;

    float distanceFromPlayer => transform.position.z - player.position.z;

    float clippingPlane => (cameraMain.transform.position.z + (distanceFromPlayer > 0 ? cameraMain.farClipPlane : cameraMain.nearClipPlane));

    float parallaxFactor => Mathf.Abs(distanceFromPlayer)/ clippingPlane;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        startZ = transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 newPosition = startPos + travel * parallaxFactor;
        transform.position = new Vector3(newPosition.x, newPosition.y, startZ);
    }
}
