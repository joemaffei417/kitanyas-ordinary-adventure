using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public GameObject checkPoint;
    public Vector3 spawnPos;
    private int checkPointNum;
    
    void Start()
    {
       spawnPos = new Vector3(this.transform.position.x, this.transform.position.y);
       checkPointNum = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("Player").transform.position.x >= this.transform.position.x - 5 && checkPointNum == 0) 
        {
            GameObject.Find("Player").GetComponent<playerControl>().spawn = spawnPos;
            checkPointNum++;
        }

    }
}
