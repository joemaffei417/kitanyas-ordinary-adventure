using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_controller : MonoBehaviour
{
    public float speed;
    public float ray;
    private bool movingR;
    public Transform ground;
    public Vector3 spawn = new Vector3(-6f, 7.5f, 0f);
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
        RaycastHit2D groundC = Physics2D.Raycast(ground.position, Vector2.down, ray);

        if (groundC.collider == false) 
        {
            if (movingR)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingR = false;
            }
            else 
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingR = true;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject.Find("Player").transform.position = spawn;
    }
}
