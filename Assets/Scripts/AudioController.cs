using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioController : MonoBehaviour
{
    public AudioSource source;
    public AudioClip clip;
    public AudioClip clip1;
    public AudioClip clip2;
    public AudioClip clip3;
    public AudioClip clip4;
    public AudioClip clip5;
    public AudioClip clip6;
    public AudioClip clip7;
    public AudioClip clip8;
    public AudioClip clip9;
    public AudioClip clip10;
    public AudioClip clip11;
    public AudioClip clip12;
    public AudioClip clip13;
    public AudioClip clip14;
    public AudioClip clip15;
    private Vector3 yeet = new Vector3(-100f, -100f, -100f);


    public GameObject nekoBean;
    public GameObject koopaBean;
    public GameObject ninaBean;
    public GameObject abiBean;

    //private Vector3 thisPosition;


    // Start is called before the first frame update
    void Start()
    {

        //thisPosition = new Vector3(this.transform.position.x, this.transform.position.y, 0);
        


    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 0)
        {
            source.PlayOneShot(clip);
            Instantiate(nekoBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);


        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 1)
        {
            source.PlayOneShot(clip1);
            Instantiate(koopaBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 2)
        {
            source.PlayOneShot(clip2);
            Instantiate(ninaBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 3)
        {
            source.PlayOneShot(clip3);
            Instantiate(abiBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 4)
        {
            source.PlayOneShot(clip4);
            Instantiate(nekoBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 5)
        {
            source.PlayOneShot(clip5);
            Instantiate(koopaBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 6)
        {
            source.PlayOneShot(clip6);
            Instantiate(ninaBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 7)
        {
            source.PlayOneShot(clip7);
            Instantiate(abiBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 8)
        {
            source.PlayOneShot(clip8);
            Instantiate(nekoBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 9)
        {
            source.PlayOneShot(clip9);
            Instantiate(koopaBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 10)
        {
            source.PlayOneShot(clip10);
            Instantiate(ninaBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 11)
        {
            source.PlayOneShot(clip11);
            Instantiate(abiBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 12)
        {
            source.PlayOneShot(clip12);
            Instantiate(nekoBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 13)
        {
            source.PlayOneShot(clip13);
            Instantiate(koopaBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 14)
        {
            source.PlayOneShot(clip14);
            Instantiate(ninaBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries++;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }

        else if (collision.collider.tag == "Player" && GameObject.Find("Player").GetComponent<playerControl>().soundSeries == 15)
        {
            source.PlayOneShot(clip15);
            Instantiate(abiBean, GameObject.Find("Main Camera").transform);
            GameObject.Find("Player").GetComponent<playerControl>().soundSeries = 0;
            transform.Translate(yeet);
            Destroy(this.gameObject, 7f);
        }


    }
}

