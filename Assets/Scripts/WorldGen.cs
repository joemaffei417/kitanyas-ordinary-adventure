using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGen : MonoBehaviour
{
    private const float playerDistance = 200f;
    [SerializeField] private Transform worldStart;
    [SerializeField] private Transform world1;
    [SerializeField] private Transform player;
    private Vector3 lastEndPosition;
    // Start is called before the first frame update
    void Start()
    {
        
    }

     void Awake()
    {
        lastEndPosition = world1.Find("LevelEnd").position;




    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(player.position, lastEndPosition) < playerDistance) 
        {
            SpawnWorld();
        }
        
    }

    private void SpawnWorld() 
    {
        Transform lastWorldTransform = SpawnWorld(lastEndPosition);
        lastEndPosition = lastWorldTransform.Find("LevelEnd").position;
    }
     Transform SpawnWorld(Vector3 position) 
    {
        Transform WorldTransform = Instantiate(world1, position, Quaternion.identity);
        return WorldTransform;
    }
}
