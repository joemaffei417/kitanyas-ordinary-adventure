using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendBeanScript : MonoBehaviour
{
    public int audioCounter;
    public int audioCounterTemp = 1;

    private GameObject player;
    private float speed = 2f;

    /*public Sprite neko;
    public Sprite koopa;
    public Sprite nina;
    public Sprite abi;*/

    //public SpriteRenderer active;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        ///audioCounter = GameObject.Find("Player").GetComponent< playerControl>().soundSeries;

       // if (audioCounter == audioCounterTemp) 
        //{
            //spriteChange();
           // audioCounterTemp++;

        //}

        Vector3 position = this.transform.position;
        position.y = Mathf.Lerp(this.transform.position.y, player.transform.position.y + 2, speed * Time.deltaTime);
        position.x = Mathf.Lerp(this.transform.position.x, player.transform.position.x - 1, speed * Time.deltaTime);

        this.transform.position = position;

        Destroy(this.gameObject, 7f);


    }

    /*void spriteChange()
    {
        if (active.sprite = null) 
        {
            active.sprite = neko;
        }
        if (active.sprite = neko)
        {
            active.sprite = koopa;
        }
        if (active.sprite = koopa)
        {
            active.sprite = nina;
        }
        if (active.sprite = nina)
        {
            active.sprite = abi;
        }
        if (active.sprite = abi)
        {
            active.sprite = neko;
        }
    }*/
}
