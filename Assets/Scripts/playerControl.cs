using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerControl : MonoBehaviour
{
    private int maxJump = 2;
    public int jumpCount = 0;
    public float movementspeed = 5.0f;
    public float jumpHeight = 20.0f;
    public Vector3 spawn = new Vector3(-6f, 7.5f, 0f);
    public int soundSeries;
    public bool isGrounded = false;

    public Animator playerAnimator;
    

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
        Movement();
        if (jumpCount < maxJump) 
        {
            Jump();
        }
        Respawn();
    }

    void Movement()
    {
        playerAnimator.SetFloat("speed", 0);
        if (Input.GetKey(KeyCode.D))
        {
            playerAnimator.SetFloat("speed", 1);
            transform.localScale = new Vector3(1, 1, 1);
            transform.position += Vector3.right * Time.deltaTime * movementspeed;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            playerAnimator.SetFloat("speed", 1);
            transform.localScale = new Vector3(-1, 1, 1);
            transform.position += Vector3.left * Time.deltaTime * movementspeed;
        }

    }

    void Jump() 
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            
            if(isGrounded == true) 
            {
                playerAnimator.SetBool("isJumping", true);
                GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpHeight;
                jumpCount++;
                
            }

        }

    }

    void Respawn() 
    {
        if (transform.position.y < -12.5) 
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            transform.position = spawn;
        }
    }


}
